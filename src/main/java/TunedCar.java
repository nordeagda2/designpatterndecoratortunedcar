package main.java;

/**
 * Created by RENT on 2017-08-14.
 */
public class TunedCar implements ICar {
    private ICar tunedCar;
    private double pressureAdded;
    private double horsePowerMultiplier;
    private boolean setCharger;
    private double engineCapacityMultiplier;


    private TunedCar(ICar tunedCar, double pressureAdded, double horsePowerMultiplier, boolean setCharger, double engineCapacityMultiplier) {
        this.tunedCar = tunedCar;
        this.pressureAdded = pressureAdded;
        this.horsePowerMultiplier = horsePowerMultiplier;
        this.setCharger = setCharger;
        this.engineCapacityMultiplier = engineCapacityMultiplier;
    }


    @Override
    public double getHorsePower() {
        return tunedCar.getHorsePower() * horsePowerMultiplier;
    }

    @Override
    public double getChargerPressure() {
        return tunedCar.getChargerPressure() + pressureAdded;
    }

    @Override
    public boolean isHasCharger() {
        return setCharger;
    }

    @Override
    public double getEngineCapacity() {
        return tunedCar.getEngineCapacity() * engineCapacityMultiplier;
    }

    @Override
    public ICar getCar() {
        return tunedCar;
    }

    @Override
    public String toString() {
        return "charger pressure: " + getChargerPressure() +
                ", horse power: " + getHorsePower() +
                ", has charger? " + isHasCharger() +
                ", engine capacity: " +getEngineCapacity();
    }

    public static class Builder {

        private ICar tunedCar;
        private double pressureAdded;
        private double horsePowerMultiplier;
        private boolean setCharger;
        private double engineCapacityMultiplier;

        public Builder setTunedCar(ICar tunedCar) {
            this.tunedCar = tunedCar;
            return this;
        }

        public Builder setPressureAdded(double pressureAdded) {
            this.pressureAdded = pressureAdded;
            return this;
        }

        public Builder setHorsePowerMultiplier(double horsePowerMultiplier) {
            this.horsePowerMultiplier = horsePowerMultiplier;
            return this;
        }

        public Builder setSetCharger(boolean setCharger) {
            this.setCharger = setCharger;
            return this;
        }

        public Builder setEngineCapacityMultiplier(double engineCapacityMultiplier) {
            this.engineCapacityMultiplier = engineCapacityMultiplier;
            return this;
        }

        public TunedCar createTunedCar() {
            return new TunedCar(tunedCar, pressureAdded, horsePowerMultiplier, setCharger, engineCapacityMultiplier);
        }
    }
}
